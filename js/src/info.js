"use strict"

var $ = require('jquery'),
    _ = require('lodash'),
    U = require('treemap/utility'),
    Bacon = require('baconjs'),
    inlineEditForm = require('treemap/inlineEditForm'),
    alerts = require('treemap/alerts'),
    History = require('history'),
    statePrompter = require('treemap/statePrompter'),
    csrf = require('treemap/csrf')

// exports.init = function(options) {
var _init = function(options) {
   // var detailUrl = window.location.href
   //  if (U.getLastUrlSegment(detailUrl) == 'edit') {
   //      detailUrl = U.removeLastUrlSegment(detailUrl)
   //  }

    // Cross-site forgery protection
    $.ajaxSetup(csrf.jqueryAjaxSetupOptions)

    var shouldBeInEditModeBus = new Bacon.Bus()
    var shouldBeInEditModeStream = shouldBeInEditModeBus.merge(
        $(window).asEventStream('popstate').map(function() {
            return U.getLastUrlSegment() === 'edit'
        })
    );

    var prompter = statePrompter.init({
        warning: options.config.trans.exitWarning,
        question: options.config.trans.exitQuestion
    })

    var form = inlineEditForm.init(
        _.extend(options.inlineEditForm, {
            config: options.config,
            // updateUrl: detailUrl,
            // onSaveBefore: onSaveBefore,
            shouldBeInEditModeStream: shouldBeInEditModeStream,
            errorCallback: alerts.makeErrorCallback(options.config)
        })
    );

    form.inEditModeProperty.onValue(function(inEditMode) {
        var hrefHasEdit = U.getLastUrlSegment() === 'edit'
        if (inEditMode) {
            prompter.lock()
            if (!hrefHasEdit) {
                History.replaceState(null, document.title, U.appendSegmentToUrl('edit'))
            }
        } else {
            prompter.unlock()
            if (hrefHasEdit) {
                History.replaceState(null, document.title, U.removeLastUrlSegment())
            }
        }
    })

    form.saveOkStream.onValue(function(data) {
        if (data.responseData.url) {
            window.location = data.responseData.url
        }
    })

    // return {
    //     inlineEditForm: form
    // }

}

exports.init = function(extraFormConfig) {
    _init({
        config: otm.settings,
        startInEditMode: false, // see old code for conditional
        inlineEditForm: _.extend({
            form: '#map-form',
            edit: '#edit-map',
            save: '#save-edit-map',
            cancel: '#cancel-edit-map',
            displayFields: '[data-class="display"]',
            editFields: '[data-class="edit"]',
            validationFields: '[data-class="error"]',
        }, extraFormConfig)
    })
}

// exports.init = function(updateUrl) {
//     _init({
//         config: otm.settings,
//         startInEditMode: false, // see old code for conditional
//         inlineEditForm: {
//             form: '#map-form',
//             edit: '#edit-map',
//             save: '#save-edit-map',
//             cancel: '#cancel-edit-map',
//             displayFields: '[data-class="display"]',
//             editFields: '[data-class="edit"]',
//             validationFields: '[data-class="error"]',
//             updateUrl: updateUrl
//         }
//     })
// }

// _init({
//     config: otm.settings,
//     startInEditMode: false, // see old code for conditional
//     inlineEditForm: {
//         form: '#map-form',
//         edit: '#edit-map',
//         save: '#save-edit-map',
//         cancel: '#cancel-edit-map',
//         displayFields: '[data-class="display"]',
//         editFields: '[data-class="edit"]',
//         validationFields: '[data-class="error"]'
//     }
// })

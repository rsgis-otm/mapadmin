"use strict"

var $ = require('jquery'),
    _ = require('lodash'),
    U = require('treemap/utility'),
    Bacon = require('baconjs'),
    inlineEditForm = require('treemap/inlineEditForm'),
    alerts = require('treemap/alerts'),
    History = require('history'),
    statePrompter = require('treemap/statePrompter'),
    csrf = require('treemap/csrf')
    // Bloodhound = require("bloodhound")
    // otmTypeahead = require('treemap/otmTypeahead'),
    // BU = require('treemap/baconUtils'),
    // Search = require('treemap/search')

var _init = function(options) {
    // Cross-site forgery protection
    $.ajaxSetup(csrf.jqueryAjaxSetupOptions)

    var shouldBeInEditModeBus = new Bacon.Bus()
    var shouldBeInEditModeStream = shouldBeInEditModeBus.merge(
        $(window).asEventStream('popstate').map(function() {
            return U.getLastUrlSegment() === 'edit'
        })
    );

    var prompter = statePrompter.init({
        warning: options.config.trans.exitWarning,
        question: options.config.trans.exitQuestion
    })

    var form = inlineEditForm.init(
        _.extend(options.inlineEditForm, {
            config: options.config,
            // updateUrl: detailUrl,
            // onSaveBefore: onSaveBefore,
            shouldBeInEditModeStream: shouldBeInEditModeStream,
            errorCallback: alerts.makeErrorCallback(options.config)
        })
    );

    form.inEditModeProperty.onValue(function(inEditMode) {
        var hrefHasEdit = U.getLastUrlSegment() === 'edit'
        if (inEditMode) {
            prompter.lock()
            if (!hrefHasEdit) {
                History.replaceState(null, document.title, U.appendSegmentToUrl('edit'))
            }
        } else {
            prompter.unlock()
            if (hrefHasEdit) {
                History.replaceState(null, document.title, U.removeLastUrlSegment())
            }
        }
    })

    form.saveOkStream.onValue(function(data) {
        if (data.responseData.url) {
            window.location = data.responseData.url
        }
    })
}

// var _initSearch = function (config) {
//     var searchStream = BU.enterOrClickEventStream({
//             inputs: 'input[data-class="user-search"]',
//             button: '#perform-user-search'
//         }),
//         filtersStream = searchStream
//             .filter(BU.isUndefinedOrEmpty)
//             .map(Search.buildSearch)
//     otmTypeahead.create({
//         name: "users",
//         url: config.usersUrl,
//         input: "#users-typeahead",
//         template: "#users-element-template",
//         hidden: "#search-users",
//         button: "#users-toggle",
//         reverse: "id",
//         forceMatch: true
//     })
//     filtersStream.onValue(_searchUsers)
// }

// var _searchUsers = function() {
//     console.log('search!')
// }

// var _initSearch = function (config) {
//     var engine = new Bloodhound({
//         name: 'users',
//         prefetch: config.usersUrl,
//         limit: 1000,
//         datumTokenizer: function(datum) {
//             return datum.tokens
//         },
//         queryTokenizer: Bloodhound.tokenizers.nonword
//         // sorter:
//     })
//     var promise = engine.initialize()
// }

exports.init = function(extraFormConfig) {
    _init({
        config: otm.settings,
        startInEditMode: false, // see old code for conditional
        inlineEditForm: _.extend({
            form: '#user-form',
            edit: '#edit-users',
            save: '#save-edit-users',
            cancel: '#cancel-edit-users',
            displayFields: '[data-class="display"]',
            editFields: '[data-class="edit"]',
            validationFields: '[data-class="error"]',
        }, extraFormConfig)
    })
    // $('#search-users-btn').on('click', function() {
    //     $(this).blur()
    //     $.getJSON(
    //         extraFormConfig.usersUrl,
    //         $('#user-search-form').serialize(),
    //         function(data, textStatus, jqXHR) {
    //             if (textStatus == 'success') {
    //                 for i in data.length
    //                 $('#user-form table').append('<tr></tr>')
    //             console.log(data)
    //             console.log(textStatus)
    //             console.log(jqXHR)
    //             }
    //         }
    //     )
    // })

    // _initSearch(extraFormConfig)
    // otmTypeahead.create({
    //     name: "users",
    //     url: extraFormConfig.usersUrl,
    //     input: "#users-typeahead",
    //     template: "#users-element-template",
    //     hidden: "#search-users",
    //     button: "#users-toggle",
    //     reverse: "id",
    //     forceMatch: true
    // })
    // var searchStream = BU.enterOrClickEventStream({
    //         inputs: 'input[data-class="user-search"]',
    //         button: '#perform-user-search'
    //     }) //,
    // searchStream.onValue(_searchUsers)
    // var stupid = ('#perform-user-search').asEventStream('click').log();
    //     filtersStream = searchStream
    //         .filter(BU.isUndefinedOrEmpty)
    //         .map(_searchUsers)
    // filtersStream.onValue(_searchUsers)
}

"use strict"

var $ = require('jquery')
    // csrf = require('treemap/csrf')

$.ajaxSetup(require('treemap/csrf').jqueryAjaxSetupOptions)
// $.ajaxSetup(csrf.jqueryAjaxSetupOptions)

$('#submit-btn').on('click', function() {
    $.ajax({
        url: window.location.href,
        data: $('#content-form').serialize(),
        method: 'PUT',
        dataType: 'json',
        success: function(data, status) {
            if (data.ok) {
                $('#result-info')
                    .removeClass('alert-danger')
                    .addClass('alert-success')
                    .html('Page saved')
                    .show()
            } else {
                $('#result-info')
                    .removeClass('alert-success')
                    .addClass('alert-danger')
                    .html('Sorry, but there was an error saving your page')
                    .show()
            }
            console.log(data)
            console.log(status)
        }
    })
})

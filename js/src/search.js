"use strict"

var $ = require('jquery'),
    // _ = require('lodash'),
    otmSearch = require('treemap/search')
    // Bacon = require('baconjs'),
    // _ = require('lodash'),
    // moment = require("moment"),
    // isTypeaheadHiddenField = require('treemap/fieldHelpers'),
    // FH = require('treemap/fieldHelpers'),
    // querystring = require('querystring');

module.exports = exports = {
    initDefaults: otmSearch.initDefaults,
    init: otmSearch.init
}

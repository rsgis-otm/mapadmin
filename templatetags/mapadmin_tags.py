from django import template

import mapadmin.util as util

register = template.Library()

@register.filter
def user_is_admin(user, instance_id):
    return util.user_is_admin(user, instance_id)

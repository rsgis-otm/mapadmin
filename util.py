from treemap.models import InstanceUser, User

def user_is_admin(user, instance):
    try:
        return _get_instance_user(user, instance).admin
    except:
        return False

def _get_instance_user(user, instance):
    if isinstance(user, InstanceUser):
        return user
    else:
        return user.get_instance_user(instance)

def _get_user(user):
    if isinstance(user, User):
        return user
    else:
        return user.user

# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division

from django.conf.urls import patterns, url
from django.contrib import admin

from otm_comments.views import comment_moderation_endpoint

from treemap import routes
from treemap.instance import URL_NAME_PATTERN
from treemap.urls import USERNAME_PATTERN

import mapadmin.views as views # import disable_user_view, map_admin_view, map_info_view, upload_view, users_view

admin.autodiscover()
instance_pattern = r'^(?P<instance_url_name>' + URL_NAME_PATTERN + r')'
pattern = instance_pattern + '/mapadmin/'

urlpatterns = patterns(
    '',
    url(r'^users/%s/$' % USERNAME_PATTERN, views.user_profile_view, name='user'),
    # url(pattern + r'$', map_admin_view, name='map_admin2'),
    url(pattern + r'$', views.map_info_view, name='map_admin'),
    url(pattern + r'info/?$', views.map_info_view, name='map_info'),
    # url(pattern + r'photos$', photo_review_endpoint, name='photos'),
    url(pattern + r'upload/?$', views.upload_view, name='upload'),
    url(pattern + r'comments/?$', comment_moderation_endpoint, name='comments'),
    url(pattern + r'users/?$', views.users_view, name='useradmin'),
    url(pattern + r'staticpages/?$', views.static_pages_view, name='staticpages'),
    # url(pattern + r'staticpage/new/?$', views.new_static_page_view, name='new_staticpage'),
    # url(pattern + r'staticpage(?:/(?P<page_id>\d+))?/?$', views.static_page_view, name='edit_staticpage'),
    url(pattern + r'staticpage/(?P<page_id>\d+)/?$', views.static_page_view, name='edit_staticpage'),
    # url(instance_pattern + r'/spp/$', views.species_view, name="friendly_species_list2"),
    # url(instance_pattern + r'/spp/(?P<format>(common)|(scientific))/?$', views.species_view, name="friendly_species_list"),
    # url(instance_pattern + r'/spp(?:/(?P<format>(common)|(scientific)))?/?$', views.species_view, name="friendly_species_list"),
    # url(pattern + r'user_list$', user_list_view, name='user_list')
    url(pattern + r'photo_review_full/$', routes.photo_review, name='photo_review_full'),
    url(instance_pattern + r'/photo_review_full/$', routes.photo_review),
    # url(pattern + r'disable/(?P<username>\w+)$', views.disable_user_view, name='disable_user'),
    # url(pattern + r'enable/(?P<username>\w+)$', views.enable_user_view, name='enable_user'),
    url(pattern + r'(?P<username>\w+)/(?P<action>(enable)|(disable))$', views.disable_user_view, name='enable_user'),
)

import json

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import QueryDict
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import ugettext as trans

from django_tinsel.decorators import json_api_call, render_template, route
from django_tinsel.utils import decorate as do

from opentreemap.util import dotted_split

from treemap.decorators import (admin_instance_request, instance_request, require_http_method,
                                return_400_if_validation_errors, json_api_edit)
from treemap.lib.map_feature import title_for_map_feature
from treemap.models import Instance, InstanceUser, StaticPage, Tree, User #Species
from treemap.util import get_instance_or_404, package_field_errors
from treemap.views.user import user as treemap_user_view
from treemap.views.user import _small_feature_photo_url
# from treemap.views.misc import species_list
from treemap import routes

# from mapadmin.models import StaticPageForm
from mapadmin.util import user_is_admin

def static_page(request, instance, page_id):
    return { 'page': StaticPage.objects.get(pk=page_id) }
    # return {'form': StaticPageForm()}

def static_pages(request, instance_url_name):
    return { 'pages': StaticPage.objects.filter(instance=instance_url_name.id) }

def update_static_page(request, instance, page_id):
    data = QueryDict(request.body)
    # print('data', data)
    page = StaticPage.objects.get(pk=page_id)
    # print(page)
    # print(json.loads(request.body)['content'])
    page.content = data['content']
    page.save()
    return { 'ok': True }
    # try:
    #     page = StaticPage.objects.get(pk=page_id)
    #     page.content = request.PUT.get('content')
    #     page.save()
    #     return { 'ok': True }
    # except:
    #     return { 'ok': False }


def map_admin(request, instance_url_name):
    pass
    # request.management_views = settings.MANAGEMENT_VIEWS


def users_admin(request, instance_url_name):
    q = _get_constraints(request)
    users = InstanceUser.objects.filter(q, instance_id=instance_url_name.id)\
        .order_by('-admin', 'user__username')
    # users = _add_constraints(request, users)
    return {
        'users': users,
        # 'users': InstanceUser.objects.filter(instance_id=instance_url_name.id, admin=True)
        #     .order_by('user__username'),
        # 'users': InstanceUser.objects.filter(instance_id=instance_url_name.id)
        #     .order_by('-admin', 'user__username'),
        'instance': request.instance
    }

def disable_user(request, instance_url_name, username, action):
    current_user = request.user
    # username = json.loads(request.body)['username']
    if username and username != current_user.username:
        user = User.objects.get(username=username)
        user.is_active = action == 'enable'
        user.save_with_user(current_user)
    return { 'ok': True }

# def enable_user(request, instance_url_name, username):
#     current_user = request.user
#     # username = json.loads(request.body)['username']
#     if username and username != current_user.username:
#         user = User.objects.get(username=username)
#         user.is_active = True
#         user.save_with_user(current_user)
#     return { 'ok': True }

def user_profile(request, username):
    context = treemap_user_view(request, username)
    context['action'] = 'disable' if context['user'].is_active else 'enable'
    # user = get_object_or_404(User, username=username)
    # sql = '''SELECT treemap_tree.*
    #          FROM treemap_tree JOIN treemap_audit ON treemap_tree.id = treemap_audit.model_id
    #          WHERE treemap_audit.model = 'Tree' AND treemap_audit.field = 'id' AND
    #                treemap_audit.action = 1 AND treemap_audit.user_id = %s
    #          ORDER BY created DESC'''
    # trees_rqs = Tree.objects.raw(sql, [user.id])
    # trees = [{
    #     'tree': tree,
    #     'title': tree.species.common_name if tree.species else trans("Missing Species"),
    #     'instance': tree.instance,
    #     'address': tree.plot.address_full,
    #     'photo': tree.photos()[0].thumbnail.url if len(tree.photos()) > 0 else None
    # } for tree in trees_rqs]
    # context['trees'] = trees
    return context


def _get_constraints(request):
    username = request.GET.get('username')
    firstname = request.GET.get('firstname')
    lastname = request.GET.get('lastname')
    q = Q(admin=True)
    if username:
        q = q | Q(user__username__icontains=username)
    if firstname:
        q = q | Q(user__first_name__icontains=firstname)
    if lastname:
        q = q | Q(user__last_name__icontains=lastname)
    return q


def map_info(request, instance_url_name):
    return {'instance': request.instance }

def update_user_info(request, instance_url_name):
    instance = request.instance
    current_user = request.user #.get_instance_user(instance)
    request_dict = json.loads(request.body)
    for (identifier, value) in request_dict.iteritems():
        if identifier[:6] == 'admin_':
            username = identifier[6:]
            if username == current_user.username:
                continue
            user = InstanceUser.objects.get(instance_id=instance.id, user__username=username)
            if value and not user.admin:
                user.admin = True
                user.save_with_user(current_user)
            elif not value and user.admin:
                user.admin = False
                user.save_with_user(current_user)
    return { 'ok': True }

def update_map_info(request, instance_url_name):
    url = instance_url_name.url_name
    result = _update_map(request, ('name', 'url_name', 'is_public'))
    instance = Instance.objects.get(pk=result['instanceId'])
    if instance.url_name != url:
        result.update({'url': reverse('map_admin2', args=[instance.url_name])})
    return result

def _update_map(request, allowed_fields):
    instance = request.instance
    possible_fields = instance._meta.get_all_field_names()
    split_template = 'Malformed request - invalid field %s'
    changed = False
    for (identifier, value) in json.loads(request.body).iteritems():
        object_name, field = dotted_split(identifier, 2,
                                          failure_format_string=split_template)
        if object_name != 'instance' or field not in possible_fields:
            raise Exception(split_template % identifier)
        if unicode(getattr(instance, field)) != value:
            if field not in allowed_fields:
                raise Exception('You may not change the value of {}'.format(field))
            setattr(instance, field, value)
            changed = True
    if changed:
        errors = {}
        try:
            instance.save()
        except ValidationError as e:
            errors.update(package_field_errors('instance', e))
        if errors:
            raise ValidationError(errors)
    return {
        'ok': True,
        'geoRevHash': instance.geo_rev_hash,
        'instanceId': instance.id
    }

def upload(request, instance_url_name):
    pass


# user_list_view = do(
#     json_api_call,
#     admin_instance_request,
#     user_search)
#     # user_list)


# species_view = do(
#     instance_request,
#     render_template('mapadmin/species.html'),
#     spp_list)

# user_profile_view = route(GET=render_template('treemap/user.html')(treemap_user_view))
# user_profile_view = do(
#     require_http_method('GET'),
#     render_template('treemap/user.html'),
#     user_profile)
user_profile_view = route(
    GET=do(render_template('treemap/user.html'), user_profile),
    ELSE=routes.user)

static_pages_view = do(
    admin_instance_request,
    render_template('mapadmin/staticpages.html'),
    static_pages)

static_page_view = do(
    admin_instance_request,
    route(
        GET=do(
            render_template('mapadmin/staticpage.html'),
            static_page),
        PUT=do(
            json_api_edit,
            return_400_if_validation_errors,
            update_static_page)))

# new_static_page_view = do(
#     admin_instance_request,
#     route(
#         GET=do(
#             render_template('mapadmin/staticpage_new.html'),
#             blank_static_page),
#         POST=do(
#             json_api_edit,
#             return_400_if_validation_errors,
#             new_static_page)))

map_admin_view = do(
    # require_http_method("GET"),
    admin_instance_request,
    render_template('mapadmin/admin.html'),
    map_admin)

disable_user_view = do(
    admin_instance_request,
    json_api_call,
    disable_user)

users_view = do(
    # require_http_method("GET"),
    admin_instance_request,
    route(
        GET=do(
            render_template('mapadmin/users.html'),
            users_admin),
        PUT=do(
            json_api_call,
            return_400_if_validation_errors,
            update_user_info)))

map_info_view = do(
    admin_instance_request,
    route(
        GET=do(
            render_template('mapadmin/mapinfo.html'),
            map_info),
        PUT=do(
            json_api_call,
            return_400_if_validation_errors,
            update_map_info)))

upload_view = do(
    admin_instance_request,
    render_template('mapadmin/imports.html'),
    upload)

# map_admin_view = do(
#     # admin_instance_request,
#     instance_request,
#     # instance_or_groups_admin,
#     route(
#         GET=do(
#             render_template('groupmap/admin.html'),
#             gmvi.get_admin_view_context),
#         ELSE=do(
#             login_or_401,
#             json_api_call,
#             route(
#                 PUT=gmvi.update_instance,
#                 DELETE=gmvi.delete_instance))))


# def user_list(request, instance_url_name):
#     max_items = request.GET.get('max_items', None)
#     users = InstanceUser.objects.filter(instance_id=instance_url_name.id).order_by('user__username')
#     if max_items:
#         users = users[:max_items]
#     def get_info(user):
#         info = {
#             'username': user.user.username,
#             'first_name': user.user.first_name,
#             'last_name': user.user.last_name
#         }
#         info.update({
#             'id': user.id,
#             'admin': user.admin,
#             'tokens': list(info.values())
#         })
#         return info
#     return [get_info(user) for user in users]


# def _add_constraints(request, qs):
#     admin = request.GET.get('admin') or 'yes'
#     username = request.GET.get('username')
#     firstname = request.GET.get('firstname')
#     lastname = request.GET.get('lastname')
#     if admin == 'yes':
#         qs = qs.filter(admin=True)
#     elif admin == 'no':
#         qs = qs.filter(admin=False)
#     if username:
#         qs = qs.filter(user__username__icontains=username)
#     if firstname:
#         qs = qs.filter(user__first_name__icontains=firstname)
#     if lastname:
#         qs = qs.filter(user__last_name__icontains=lastname)
#     return qs

# def user_search(request, instance_url_name):
#     username = request.GET.get('username')
#     firstname = request.GET.get('firstname')
#     lastname = request.GET.get('lastname')
#     admin = request.GET.get('include_admins')
#     users = InstanceUser.objects.filter(instance_id=instance_url_name.id) #.order_by('user__username')
#     if username:
#         users = users.filter(user__username__icontains=username)
#     if firstname:
#         users = users.filter(user__first_name__icontains=firstname)
#     if lastname:
#         users = users.filter(user__last_name__icontains=lastname)
#     if not admin:
#         users = users.filter(admin=False)
#     def get_info(user):
#         return {
#             'id': user.id,
#             'username': user.user.username,
#             'first_name': user.user.first_name,
#             'last_name': user.user.last_name,
#             'admin': user.admin,
#         }
#     return [get_info(user) for user in users]


    # users_qs = instance.scope_model(User)\
    #                    .order_by('username')\
    #                    .values('username', 'first_name', 'last_name')
    # if max_items:
    #     users_qs = users_qs[:max_items]
    # for user in users_qs:
    #     user.update({'tokens': list(user.values())})
    # print(users_qs)
    # return list(users_qs)



# def spp_list(request, instance, format='common'):
#     # def get_name(spp):
#     #     return '{} [{}]'.format(spp['common_name'], spp['scientific_name'])
#     # def get_sci_name(spp):
#     #     return '{} [{}]'.format(spp['scientific_name'], spp['common_name'])
#     def get_data(spp): #, f):
#         q = {}
#         for field in ['genus', 'species', 'cultivar']:
#             if spp[field]:
#                 q['species.' + field] = {'IS': spp[field]}
#         return {'common': spp['common_name'],
#                 'scientific': spp['scientific_name'],
#                 'query': json.dumps(q)}
#         # return {'name': f(spp), 'query': json.dumps(q)}
#     if format == 'scientific':
#         # f = get_sci_name
#         other_format = 'common'
#     else:
#         # f = get_name
#         format = 'common'
#         other_format = 'scientific'
#     return {'format': format,
#             'other_format': other_format,
#             'species': sorted([get_data(s) for s in species_list(request, instance)], key=lambda s: s[format])}
#             # 'species': sorted([get_data(s, f) for s in species_list(request, instance)], key=lambda s: s['name'])}
#             # 'species': sorted([f(s) for s in species_list(request, instance)])}

# def spp_list(request, instance, format='common'):
#     if format == 'scientific':
#         other_format = 'common'
#     else:
#         format = 'common'
#         other_format = 'scientific'

#     def get_data(spp):
#         q = {}
#         for field in ['genus', 'species', 'cultivar']:
#             if spp[field]:
#                 q['species.' + field] = {'IS': spp[field]}
#         return {'name': spp[format + '_name'],
#                 'other_name': spp[other_format + '_name'],
#                 'symbol':
#                 'query': json.dumps(q)}

#     return {'format': format,
#             'other_format': other_format,
#             'species': sorted([get_data(s) for s in species_list(request, instance)], key=lambda s: s['name'])}
#             # 'species': sorted([get_data(s, f) for s in species_list(request, instance)], key=lambda s: s['name'])}


# def spp_list(request, instance, format='common'):
#     common = format == 'common'
#     def get_data(spp):
#         q = {}
#         for field in ['genus', 'species', 'cultivar']:
#             if spp[field]:
#                 q['species.' + field] = {'IS': spp[field]}
#         sci_name = Species.get_scientific_name(spp['genus'],
#                                                spp['species'],
#                                                spp['cultivar'])
#         return {'name': spp['common_name'] if common else sci_name,
#                 'other_name': sci_name if common else spp['common_name'],
#                 'symbol': spp['otm_code'],
#                 'query': json.dumps(q)}

#     species_qs = instance.scope_model(Species)\
#                          .order_by('common_name' if common else 'genus','species','cultivar')\
#                          .values('common_name', 'genus',
#                                  'species', 'cultivar', 'id', 'otm_code')
#     return {'format': format,
#             'other_format': 'scientific' if common else 'common',
#             'species': [get_data(s) for s in species_qs]}
#             # 'species': sorted([get_data(s, f) for s in species_list(request, instance)], key=lambda s: s['name'])}
#             # 'species': sorted([f
